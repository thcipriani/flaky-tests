#!/usr/bin/env bash

# Run this to update everything in this repo
BEFORE=$(echo 'select count(*) from testcases;' | sqlite3 data/tests.db)
python3 fetchtests.py --refresh

jupyter nbconvert --to notebook --execute README.ipynb --inplace
rm README.md
make README.md

AFTER=$(echo 'select count(*) from testcases;' | sqlite3 data/tests.db)
git add data/tests.db README.ipynb README.md
git commit -m "$(date --rfc-email) (${AFTER} testcases)"
