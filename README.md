# Flaky JUnit tests

The `fetchtests.py` script in this repo (invoked as `python3 fetchtests.py --refresh`):
1. Fetches the 50 latest builds for all jobs in [our jenkins](https://integration.wikimedia.org/ci)
2. Finds all XML files in build artifacts
3. Attempts to parse the XML files as JUnit files
4. Stores any jobs, builds (including SHA1s under test), testsuites, and testcases it finds in jobs that store JUnit artifacts 


```python
import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns

from sqlalchemy import create_engine
from IPython.display import display, Markdown

# Makes your data 538% better...I think
plt.style.use('fivethirtyeight')


engine = create_engine('sqlite:///data/tests.db')
connection = engine.connect()

# This should be empty unless we've somehow duplicated builds
df = pd.read_sql('select j.name, b.build_id, count(*) c from jobs j join builds b on b.job_id = j.id GROUP BY j.name, b.build_id HAVING c > 1;', con=connection)
display(Markdown('This number should be 0 unless there are duplicate builds: **{}**'.format(len(df))))

```


This number should be 0 unless there are duplicate builds: **0**


## Repos that have JUnit results


```python
df = pd.read_sql('SELECT repo, count(*) AS count FROM builds GROUP BY repo', con=connection)
display(Markdown(df.sort_values(by="count", ascending=False).to_markdown()))
```


|     | repo                                                |   count |
|----:|:----------------------------------------------------|--------:|
|   2 | mediawiki/core                                      |     729 |
| 122 | mediawiki/extensions/Wikibase                       |     412 |
| 146 | mediawiki/skins/Vector                              |     153 |
|  66 | mediawiki/extensions/GrowthExperiments              |     146 |
| 143 | mediawiki/skins/MinervaNeue                         |     139 |
|  81 | mediawiki/extensions/MobileFrontend                 |     130 |
| 117 | mediawiki/extensions/VisualEditor                   |     115 |
| 110 | mediawiki/extensions/Translate                      |      94 |
| 149 | wikimedia/fundraising/crm                           |      83 |
|  50 | mediawiki/extensions/DonationInterface              |      70 |
|  51 | mediawiki/extensions/Echo                           |      64 |
|  35 | mediawiki/extensions/CirrusSearch                   |      59 |
| 124 | mediawiki/extensions/WikibaseLexeme                 |      57 |
|  45 | mediawiki/extensions/ContentTranslation             |      50 |
| 120 | mediawiki/extensions/WikiLambda                     |      48 |
|  77 | mediawiki/extensions/Math                           |      47 |
|   4 | mediawiki/extensions/AdvancedSearch                 |      42 |
|  93 | mediawiki/extensions/Popups                         |      40 |
|  32 | mediawiki/extensions/CentralNotice                  |      39 |
|  84 | mediawiki/extensions/Newsletter                     |      39 |
| 112 | mediawiki/extensions/TwoColConflict                 |      37 |
|  97 | mediawiki/extensions/RelatedArticles                |      33 |
|  40 | mediawiki/extensions/Collection                     |      33 |
|   0 | integration/config                                  |      33 |
|  48 | mediawiki/extensions/DiscussionTools                |      32 |
| 135 | mediawiki/extensions/cldr                           |      30 |
| 109 | mediawiki/extensions/TimedMediaHandler              |      25 |
| 101 | mediawiki/extensions/SecurePoll                     |      24 |
| 132 | mediawiki/extensions/WikimediaMessages              |      22 |
|  94 | mediawiki/extensions/ProofreadPage                  |      22 |
|  36 | mediawiki/extensions/Cite                           |      22 |
|  34 | mediawiki/extensions/ChessBrowser                   |      21 |
| 139 | mediawiki/services/parsoid                          |      21 |
| 127 | mediawiki/extensions/WikibaseQualityConstraints     |      19 |
| 126 | mediawiki/extensions/WikibaseMediaInfo              |      19 |
|  58 | mediawiki/extensions/FlaggedRevs                    |      19 |
|  63 | mediawiki/extensions/Gadgets                        |      16 |
|  79 | mediawiki/extensions/MediaSearch                    |      15 |
|  52 | mediawiki/extensions/EventBus                       |      15 |
|  60 | mediawiki/extensions/Flow                           |      14 |
|  16 | mediawiki/extensions/BlueSpiceFoundation            |      14 |
|  55 | mediawiki/extensions/ExternalData                   |      14 |
| 137 | mediawiki/extensions/timeline                       |      14 |
|  87 | mediawiki/extensions/OOJSPlus                       |      13 |
|  53 | mediawiki/extensions/EventLogging                   |      13 |
| 147 | mediawiki/skins/WMAU                                |      12 |
|  67 | mediawiki/extensions/IPInfo                         |      12 |
|   3 | mediawiki/extensions/AbuseFilter                    |      12 |
|  12 | mediawiki/extensions/BlueSpiceBookshelf             |      11 |
| 150 | wikimedia/fundraising/crm/civicrm                   |      11 |
|  38 | mediawiki/extensions/Cognate                        |      10 |
|  70 | mediawiki/extensions/InterwikiSorting               |      10 |
|   9 | mediawiki/extensions/BlockInactive                  |       9 |
|  59 | mediawiki/extensions/FlexDiagrams                   |       9 |
| 106 | mediawiki/extensions/TemplateData                   |       9 |
| 113 | mediawiki/extensions/UniversalLanguageSelector      |       9 |
|  86 | mediawiki/extensions/OAuth                          |       8 |
|  26 | mediawiki/extensions/BlueSpiceUEModulePDF           |       8 |
| 129 | mediawiki/extensions/WikimediaBadges                |       8 |
|   1 | integration/quibble                                 |       7 |
|  24 | mediawiki/extensions/BlueSpiceUEModuleBookPDF       |       7 |
|  31 | mediawiki/extensions/CentralAuth                    |       7 |
|  27 | mediawiki/extensions/BlueSpiceUniversalExport       |       7 |
|  74 | mediawiki/extensions/MachineVision                  |       6 |
|   8 | mediawiki/extensions/BetaFeatures                   |       6 |
|  89 | mediawiki/extensions/PageTriage                     |       6 |
|  73 | mediawiki/extensions/Linter                         |       6 |
|  25 | mediawiki/extensions/BlueSpiceUEModuleHTML          |       6 |
|  98 | mediawiki/extensions/ReplaceText                    |       6 |
|  22 | mediawiki/extensions/BlueSpiceReaders               |       6 |
| 142 | mediawiki/skins/Cosmos                              |       6 |
|  54 | mediawiki/extensions/EventStreamConfig              |       6 |
| 140 | mediawiki/skins/BlueSpiceCalumma                    |       6 |
| 144 | mediawiki/skins/Poncho                              |       6 |
| 130 | mediawiki/extensions/WikimediaEvents                |       6 |
|  20 | mediawiki/extensions/BlueSpicePageTemplates         |       5 |
|  42 | mediawiki/extensions/CommonsMetadata                |       5 |
|  65 | mediawiki/extensions/GlobalWatchlist                |       5 |
| 148 | mediawiki/vendor                                    |       5 |
|  90 | mediawiki/extensions/PageViewInfo                   |       5 |
|  23 | mediawiki/extensions/BlueSpiceSaferEdit             |       4 |
|  68 | mediawiki/extensions/InputBox                       |       4 |
|  91 | mediawiki/extensions/ParserFunctions                |       4 |
|  92 | mediawiki/extensions/PdfHandler                     |       4 |
|  29 | mediawiki/extensions/BlueSpiceWhoIsOnline           |       4 |
|  28 | mediawiki/extensions/BlueSpiceVisualEditorConnector |       4 |
| 141 | mediawiki/skins/CologneBlue                         |       4 |
|  44 | mediawiki/extensions/ContentTransfer                |       4 |
| 107 | mediawiki/extensions/Thanks                         |       4 |
| 145 | mediawiki/skins/Timeless                            |       4 |
|  10 | mediawiki/extensions/BlueSpiceAuthors               |       4 |
|  17 | mediawiki/extensions/BlueSpiceInsertLink            |       4 |
|  14 | mediawiki/extensions/BlueSpiceExpiry                |       4 |
|  11 | mediawiki/extensions/BlueSpiceAvatars               |       4 |
| 125 | mediawiki/extensions/WikibaseManifest               |       4 |
| 114 | mediawiki/extensions/UploadWizard                   |       3 |
| 115 | mediawiki/extensions/UrlShortener                   |       3 |
| 151 | wikimedia/fundraising/crm/vendor                    |       3 |
|  72 | mediawiki/extensions/Kartographer                   |       3 |
|  46 | mediawiki/extensions/ContributionScores             |       3 |
|  39 | mediawiki/extensions/CognitiveProcessDesigner       |       3 |
|  49 | mediawiki/extensions/DisplayTitle                   |       3 |
|   7 | mediawiki/extensions/Babel                          |       3 |
|  43 | mediawiki/extensions/ConfirmEdit                    |       3 |
|  88 | mediawiki/extensions/PageForms                      |       3 |
|  15 | mediawiki/extensions/BlueSpiceExportTables          |       3 |
|  99 | mediawiki/extensions/RevisionSlider                 |       2 |
|  69 | mediawiki/extensions/Interwiki                      |       2 |
|  64 | mediawiki/extensions/GlobalPreferences              |       2 |
| 123 | mediawiki/extensions/WikibaseCirrusSearch           |       2 |
| 116 | mediawiki/extensions/VEForAll                       |       2 |
|  41 | mediawiki/extensions/CommentStreams                 |       2 |
| 119 | mediawiki/extensions/WikiEditor                     |       2 |
|  47 | mediawiki/extensions/CookieWarning                  |       2 |
| 131 | mediawiki/extensions/WikimediaMaintenance           |       2 |
| 136 | mediawiki/extensions/examples                       |       2 |
| 133 | mediawiki/extensions/Wikisource                     |       2 |
|  19 | mediawiki/extensions/BlueSpicePageAssignments       |       2 |
| 134 | mediawiki/extensions/WikispeechSpeechDataCollector  |       2 |
|  75 | mediawiki/extensions/MassEditRegex                  |       2 |
| 100 | mediawiki/extensions/Scribunto                      |       2 |
|  21 | mediawiki/extensions/BlueSpicePermissionManager     |       2 |
| 111 | mediawiki/extensions/TranslationNotifications       |       2 |
|  95 | mediawiki/extensions/QuickSurveys                   |       2 |
|   5 | mediawiki/extensions/ApprovedRevs                   |       2 |
|  18 | mediawiki/extensions/BlueSpiceNamespaceCSS          |       2 |
|  85 | mediawiki/extensions/OATHAuth                       |       2 |
|  13 | mediawiki/extensions/BlueSpiceDistributionConnector |       2 |
|   6 | mediawiki/extensions/ArticlePlaceholder             |       1 |
|  33 | mediawiki/extensions/Challenge                      |       1 |
|  37 | mediawiki/extensions/CodeReview                     |       1 |
| 138 | mediawiki/selenium                                  |       1 |
|  30 | mediawiki/extensions/Cargo                          |       1 |
|  96 | mediawiki/extensions/Quiz                           |       1 |
|  61 | mediawiki/extensions/FundraisingEmailUnsubscribe    |       1 |
| 128 | mediawiki/extensions/Wikidata.org                   |       1 |
|  82 | mediawiki/extensions/MyVariables                    |       1 |
| 102 | mediawiki/extensions/SoftRedirector                 |       1 |
| 103 | mediawiki/extensions/SpamDiffTool                   |       1 |
| 104 | mediawiki/extensions/StructuredNavigation           |       1 |
| 105 | mediawiki/extensions/SyntaxHighlight_GeSHi          |       1 |
| 108 | mediawiki/extensions/TheWikipediaLibrary            |       1 |
|  83 | mediawiki/extensions/NSFileRepo                     |       1 |
|  80 | mediawiki/extensions/MergeArticles                  |       1 |
|  56 | mediawiki/extensions/FileExporter                   |       1 |
|  78 | mediawiki/extensions/MathSearch                     |       1 |
|  71 | mediawiki/extensions/JsonConfig                     |       1 |
| 118 | mediawiki/extensions/WebAuthn                       |       1 |
| 121 | mediawiki/extensions/WikiSEO                        |       1 |
|  62 | mediawiki/extensions/GWToolset                      |       1 |
|  57 | mediawiki/extensions/FileImporter                   |       1 |
|  76 | mediawiki/extensions/MassMessage                    |       1 |


# Flaky tests

Now let's try to actually find testcases that have run twice on the same SHA1 with different results


```python
tmp_table_sql = '''
CREATE TEMP TABLE gah_so_flaky AS
    SELECT
        b.repo,
        sha,
        b.url,
        ts.url,
        REPLACE(REPLACE(tc.name, ":", "_"),"|","_") as tc_name,
        min(failure) minf,
        max(failure) maxf
    FROM builds b
    JOIN testsuites ts ON ts.build_id = b.id
    JOIN testcases tc ON tc.testsuite_id = ts.id
    GROUP BY tc.name, sha;
'''
connection.execute(tmp_table_sql)
df = pd.read_sql('select * from gah_so_flaky where minf != maxf and sha != "master";', con=connection)

display(Markdown(df.to_markdown()))
```


|    | repo                                   | sha                                      | url                                                                                     | url:1                                                                                                                                       | tc_name                                                                                  |   minf |   maxf |
|---:|:---------------------------------------|:-----------------------------------------|:----------------------------------------------------------------------------------------|:--------------------------------------------------------------------------------------------------------------------------------------------|:-----------------------------------------------------------------------------------------|-------:|-------:|
|  0 | mediawiki/extensions/Wikibase          | cd5bad87908751e8dcc03cd4baf3273b9becc61e | https://integration.wikimedia.org/ci/job/wmf-quibble-apache-selenium-php72-docker/3318/ | https://integration.wikimedia.org/ci/job/wmf-quibble-apache-selenium-php72-docker/3318/artifact/log/WDIO.xunit-2021-09-28T16-41-56-282Z.xml | Clicking on a watchstar toggles the watchstar                                            |      0 |      1 |
|  1 | mediawiki/extensions/Wikibase          | b10fa969b5cf9c6441ea8411e688f14927c0652e | https://integration.wikimedia.org/ci/job/legacy-quibble-rubyselenium-docker/2451/       | https://integration.wikimedia.org/ci/job/legacy-quibble-rubyselenium-docker/2451/artifact/log/junit/TEST-features-header.xml                | Save label, description and aliases (outline example _ _ click the header save button _) |      0 |      1 |
|  2 | mediawiki/core                         | 0d37558533a8c9cd6ace66703f9f46229d70fd9a | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115467/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115467/artifact/log/WDIO.xunit-2021-09-28T17-25-57-432Z.xml      | should be able to create account                                                         |      0 |      1 |
|  3 | mediawiki/core                         | 1048f1ef249e2b206da432bab81c965f36a10bae | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115750/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115750/artifact/log/WDIO.xunit-2021-09-29T21-12-09-546Z.xml      | should be able to create account                                                         |      0 |      1 |
|  4 | mediawiki/core                         | 154bcc09258386b8e014a48317afca726d5b90d0 | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115480/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115480/artifact/log/WDIO.xunit-2021-09-28T18-11-10-369Z.xml      | should be able to create account                                                         |      0 |      1 |
|  5 | mediawiki/core                         | 1bd3a8d1f8af1a5f8167ba082c0027844d0c0767 | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115746/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115746/artifact/log/WDIO.xunit-2021-09-29T21-00-03-798Z.xml      | should be able to create account                                                         |      0 |      1 |
|  6 | mediawiki/core                         | 40e3bb999083238e15d3c8bdf54ed1c3b74b9dda | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115491/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115491/artifact/log/WDIO.xunit-2021-09-28T19-08-00-969Z.xml      | should be able to create account                                                         |      0 |      1 |
|  7 | mediawiki/core                         | 4b6f8f6ea96eeb9b27e4b427b903e023b7be4c95 | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115407/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115407/artifact/log/WDIO.xunit-2021-09-28T13-26-25-594Z.xml      | should be able to create account                                                         |      0 |      1 |
|  8 | mediawiki/core                         | 5508c39fb4d3843eb234eac30c43e372ce80a38d | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115424/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115424/artifact/log/WDIO.xunit-2021-09-28T14-23-03-122Z.xml      | should be able to create account                                                         |      0 |      1 |
|  9 | mediawiki/core                         | 55cc264642d93f2f5458573e7d4a63c0a1db26e1 | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115216/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115216/artifact/log/WDIO.xunit-2021-09-27T20-25-55-194Z.xml      | should be able to create account                                                         |      0 |      1 |
| 10 | mediawiki/core                         | 5f69eec492ae570104dbd34d1db7434552145f91 | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115220/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115220/artifact/log/WDIO.xunit-2021-09-27T20-47-08-334Z.xml      | should be able to create account                                                         |      0 |      1 |
| 11 | mediawiki/core                         | 622b72deec34a52c5fbe8705396a23ae08984a54 | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115257/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115257/artifact/log/WDIO.xunit-2021-09-27T22-34-43-705Z.xml      | should be able to create account                                                         |      0 |      1 |
| 12 | mediawiki/core                         | 83965cffcb2edec79e19eee3ca2c99365d6609f9 | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115740/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115740/artifact/log/WDIO.xunit-2021-09-29T20-47-05-211Z.xml      | should be able to create account                                                         |      0 |      1 |
| 13 | mediawiki/core                         | 9f6d6073e1d2c87703f45fbc35aca34fbc65c2e7 | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115501/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115501/artifact/log/WDIO.xunit-2021-09-28T19-48-43-369Z.xml      | should be able to create account                                                         |      0 |      1 |
| 14 | mediawiki/core                         | c377192982c701cfd16b1b4718c6cf95bfd4543b | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115736/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115736/artifact/log/WDIO.xunit-2021-09-29T20-29-55-706Z.xml      | should be able to create account                                                         |      0 |      1 |
| 15 | mediawiki/core                         | cc843bb0a8cb5afdafffc78e7815d402fe099520 | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115215/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115215/artifact/log/WDIO.xunit-2021-09-27T20-17-39-620Z.xml      | should be able to create account                                                         |      0 |      1 |
| 16 | mediawiki/core                         | d59066cd3e0162b936817dc07bc1299b06badc04 | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115493/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115493/artifact/log/WDIO.xunit-2021-09-28T19-16-36-929Z.xml      | should be able to create account                                                         |      0 |      1 |
| 17 | mediawiki/core                         | e1bfa98e98a8b03818c1ed69cbb7f7037521c24e | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115418/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115418/artifact/log/WDIO.xunit-2021-09-28T14-04-45-471Z.xml      | should be able to create account                                                         |      0 |      1 |
| 18 | mediawiki/core                         | fcaeab1f56318dda8d4b48fbe101174e805bf060 | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115693/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115693/artifact/log/WDIO.xunit-2021-09-29T16-27-43-194Z.xml      | should be able to create account                                                         |      0 |      1 |
| 19 | mediawiki/core                         | fe9a9dd6028174a6191932241892b0e09bee42b4 | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115425/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115425/artifact/log/WDIO.xunit-2021-09-28T14-41-46-914Z.xml      | should be able to create account                                                         |      0 |      1 |
| 20 | mediawiki/core                         | 0d37558533a8c9cd6ace66703f9f46229d70fd9a | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115467/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115467/artifact/log/WDIO.xunit-2021-09-28T17-25-57-432Z.xml      | should be able to log in @daily                                                          |      0 |      1 |
| 21 | mediawiki/core                         | 1048f1ef249e2b206da432bab81c965f36a10bae | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115750/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115750/artifact/log/WDIO.xunit-2021-09-29T21-12-09-546Z.xml      | should be able to log in @daily                                                          |      0 |      1 |
| 22 | mediawiki/core                         | 154bcc09258386b8e014a48317afca726d5b90d0 | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115480/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115480/artifact/log/WDIO.xunit-2021-09-28T18-11-10-369Z.xml      | should be able to log in @daily                                                          |      0 |      1 |
| 23 | mediawiki/core                         | 1bd3a8d1f8af1a5f8167ba082c0027844d0c0767 | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115746/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115746/artifact/log/WDIO.xunit-2021-09-29T21-00-03-798Z.xml      | should be able to log in @daily                                                          |      0 |      1 |
| 24 | mediawiki/extensions/WikimediaMessages | 1c60119a752a1e99b49ef4ff00484d33a67d922e | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115430/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115430/artifact/log/WDIO.xunit-2021-09-28T14-50-07-874Z.xml      | should be able to log in @daily                                                          |      0 |      1 |
| 25 | mediawiki/core                         | 40e3bb999083238e15d3c8bdf54ed1c3b74b9dda | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115491/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115491/artifact/log/WDIO.xunit-2021-09-28T19-08-00-969Z.xml      | should be able to log in @daily                                                          |      0 |      1 |
| 26 | mediawiki/core                         | 4b6f8f6ea96eeb9b27e4b427b903e023b7be4c95 | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115407/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115407/artifact/log/WDIO.xunit-2021-09-28T13-26-25-594Z.xml      | should be able to log in @daily                                                          |      0 |      1 |
| 27 | mediawiki/core                         | 5508c39fb4d3843eb234eac30c43e372ce80a38d | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115424/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115424/artifact/log/WDIO.xunit-2021-09-28T14-23-03-122Z.xml      | should be able to log in @daily                                                          |      0 |      1 |
| 28 | mediawiki/core                         | 55cc264642d93f2f5458573e7d4a63c0a1db26e1 | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115216/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115216/artifact/log/WDIO.xunit-2021-09-27T20-25-55-194Z.xml      | should be able to log in @daily                                                          |      0 |      1 |
| 29 | mediawiki/core                         | 5f69eec492ae570104dbd34d1db7434552145f91 | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115220/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115220/artifact/log/WDIO.xunit-2021-09-27T20-47-08-334Z.xml      | should be able to log in @daily                                                          |      0 |      1 |
| 30 | mediawiki/core                         | 622b72deec34a52c5fbe8705396a23ae08984a54 | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115257/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115257/artifact/log/WDIO.xunit-2021-09-27T22-34-43-705Z.xml      | should be able to log in @daily                                                          |      0 |      1 |
| 31 | mediawiki/core                         | 83965cffcb2edec79e19eee3ca2c99365d6609f9 | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115740/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115740/artifact/log/WDIO.xunit-2021-09-29T20-47-05-211Z.xml      | should be able to log in @daily                                                          |      0 |      1 |
| 32 | mediawiki/core                         | 9f6d6073e1d2c87703f45fbc35aca34fbc65c2e7 | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115501/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115501/artifact/log/WDIO.xunit-2021-09-28T19-48-43-369Z.xml      | should be able to log in @daily                                                          |      0 |      1 |
| 33 | mediawiki/core                         | c377192982c701cfd16b1b4718c6cf95bfd4543b | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115736/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115736/artifact/log/WDIO.xunit-2021-09-29T20-29-55-706Z.xml      | should be able to log in @daily                                                          |      0 |      1 |
| 34 | mediawiki/core                         | cc843bb0a8cb5afdafffc78e7815d402fe099520 | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115215/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115215/artifact/log/WDIO.xunit-2021-09-27T20-17-39-620Z.xml      | should be able to log in @daily                                                          |      0 |      1 |
| 35 | mediawiki/core                         | d59066cd3e0162b936817dc07bc1299b06badc04 | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115493/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115493/artifact/log/WDIO.xunit-2021-09-28T19-16-36-929Z.xml      | should be able to log in @daily                                                          |      0 |      1 |
| 36 | mediawiki/core                         | e1bfa98e98a8b03818c1ed69cbb7f7037521c24e | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115418/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115418/artifact/log/WDIO.xunit-2021-09-28T14-04-45-471Z.xml      | should be able to log in @daily                                                          |      0 |      1 |
| 37 | mediawiki/core                         | fcaeab1f56318dda8d4b48fbe101174e805bf060 | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115693/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115693/artifact/log/WDIO.xunit-2021-09-29T16-27-43-194Z.xml      | should be able to log in @daily                                                          |      0 |      1 |
| 38 | mediawiki/core                         | fe9a9dd6028174a6191932241892b0e09bee42b4 | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115425/      | https://integration.wikimedia.org/ci/job/wmf-quibble-selenium-php72-docker/115425/artifact/log/WDIO.xunit-2021-09-28T14-41-46-914Z.xml      | should be able to log in @daily                                                          |      0 |      1 |
| 39 | mediawiki/extensions/Wikibase          | 399ac33c2cb43edce66bcc415a9830526161274c | https://integration.wikimedia.org/ci/job/wmf-quibble-apache-selenium-php72-docker/3320/ | https://integration.wikimedia.org/ci/job/wmf-quibble-apache-selenium-php72-docker/3320/artifact/log/WDIO.xunit-2021-09-28T23-38-42-353Z.xml | shows dismissible help banner                                                            |      0 |      1 |
| 40 | mediawiki/extensions/Wikibase          | 5334bdd3c0fea5167e3d8c3ae282022df5bc7cc9 | https://integration.wikimedia.org/ci/job/wmf-quibble-apache-selenium-php72-docker/3302/ | https://integration.wikimedia.org/ci/job/wmf-quibble-apache-selenium-php72-docker/3302/artifact/log/WDIO.xunit-2021-09-28T08-38-49-996Z.xml | shows dismissible help banner                                                            |      0 |      1 |
| 41 | mediawiki/extensions/Wikibase          | 9078ea674ac6c80164a307b035fb87ceb98430f4 | https://integration.wikimedia.org/ci/job/wmf-quibble-apache-selenium-php72-docker/3323/ | https://integration.wikimedia.org/ci/job/wmf-quibble-apache-selenium-php72-docker/3323/artifact/log/WDIO.xunit-2021-09-29T10-47-55-962Z.xml | shows dismissible help banner                                                            |      0 |      1 |
| 42 | mediawiki/extensions/Wikibase          | d9ca163e16ccc73c1b8e91e102a8daa2eb370b2d | https://integration.wikimedia.org/ci/job/wmf-quibble-apache-selenium-php72-docker/3333/ | https://integration.wikimedia.org/ci/job/wmf-quibble-apache-selenium-php72-docker/3333/artifact/log/WDIO.xunit-2021-09-30T08-16-59-541Z.xml | shows dismissible help banner                                                            |      0 |      1 |
| 43 | wikimedia/fundraising/crm              | 3897da7a394ec8c8d2013e5278de32777c9767c7 | https://integration.wikimedia.org/ci/job/wikimedia-fundraising-civicrm-docker/6032/     | https://integration.wikimedia.org/ci/job/wikimedia-fundraising-civicrm-docker/6032/artifact/log/junit-phpunit.xml                           | testDuplicateHandling                                                                    |      0 |      1 |
| 44 | wikimedia/fundraising/crm              | 9706a00cf30b13c6cf47b608ea1c7935fb74c6d5 | https://integration.wikimedia.org/ci/job/wikimedia-fundraising-civicrm-docker/5989/     | https://integration.wikimedia.org/ci/job/wikimedia-fundraising-civicrm-docker/5989/artifact/log/junit-phpunit.xml                           | testDuplicateHandling                                                                    |      0 |      1 |
| 45 | wikimedia/fundraising/crm              | 9706a00cf30b13c6cf47b608ea1c7935fb74c6d5 | https://integration.wikimedia.org/ci/job/wikimedia-fundraising-civicrm-docker/5989/     | https://integration.wikimedia.org/ci/job/wikimedia-fundraising-civicrm-docker/5989/artifact/log/junit-phpunit.xml                           | testImport                                                                               |      0 |      1 |
| 46 | wikimedia/fundraising/crm              | 9706a00cf30b13c6cf47b608ea1c7935fb74c6d5 | https://integration.wikimedia.org/ci/job/wikimedia-fundraising-civicrm-docker/5989/     | https://integration.wikimedia.org/ci/job/wikimedia-fundraising-civicrm-docker/5989/artifact/log/junit-phpunit.xml                           | testRender                                                                               |      0 |      1 |



```python

```
