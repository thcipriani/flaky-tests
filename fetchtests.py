#!/usr/bin/env python3

import pdb
import argparse
import json
import lxml
import os
import requests

from dateutil import parser
import junitparser
from junitparser import JUnitXml, TestSuite, Skipped, Failure
import sqlite3

JENKINS_URL = 'https://integration.wikimedia.org/ci/api/json?tree=jobs[name,url,builds[id,url,timestamp,duration,builtOn,actions[buildsByBranchName[revision[SHA1]],parameters[name,value]],artifacts[relativePath]]]'
DB_PATH = './data/tests.db'
SEEN_JOBS = None
SEEN_BUILDS = None


def insert_job(crs, name, url):
    """
    Keep a cache of job ids.

    This is a hack. I want to insert a job ONLY IF the job has junit build
    artifacts. BUT I want to insert builds one at a time into the database
    so I don't have to hold huge amounts of parsed XML in memory.

    Anyway, it's hacky but it works and I'm tired. Fuck you for judging me.
    """
    global SEEN_JOBS
    if SEEN_JOBS is None:
        crs.execute('SELECT id, name FROM jobs;')
        SEEN_JOBS = {item[1]: item[0] for item in crs.fetchall()}

    rid = SEEN_JOBS.get(name)
    if rid is not None:
        return rid

    crs.execute('''
        INSERT INTO jobs(
            name,
            url
        ) VALUES(?,?)''', (
            name,
            url
        )
    )
    rid = crs.lastrowid
    SEEN_JOBS[name] = rid
    return rid


def build_seen(crs, url):
    global SEEN_BUILDS
    if SEEN_BUILDS is None:
        crs.execute('SELECT url FROM builds;')
        SEEN_BUILDS = [item[0] for item in crs.fetchall()]

    return url in SEEN_BUILDS


def fetch_jobs(refresh=False):
    if not os.path.exists('jobs.json') or refresh:
        r = requests.get(JENKINS_URL)
        r.raise_for_status()
        jobs = r.json()['jobs']

        with open('jobs.json', 'w') as f:
            f.write(json.dumps(jobs))

    with open('jobs.json') as f:
        jobs = json.load(f)

    return jobs


def hasattrs(obj, attrs):
    for attr in attrs:
        if not hasattr(obj, attr):
            return False
        if getattr(obj, attr) is None:
            return False
    return True


def get_timestamp(testsuite):
    if not hasattr(testsuite, 'timestamp') or testsuite.timestamp is None:
        return None
    return int(parser.parse(testsuite.timestamp, ignoretz=True).timestamp())

def get_time(testsuite):
    if not hasattr(testsuite, 'time') or testsuite.time is None:
        return None
    return float(testsuite.time)

def get_skipped(testsuite):
    # You have got to be fucking kidding me
    if hasattr(testsuite, 'skipped') or teststuie.skipped is None:
        return int(testsuite.skipped)
    if hasattr(testsuite, 'skip') or testsuite.skip is None:
        return int(testsuite.skip)
    return None

def get_classname(testcase):
    if hasattr(testcase, 'classname'):
        return testcase.classname
    return None

def parse_junit(artifact_url, junit):
    """
    Takes a build as JUnitXml and parses relevant data
    """
    if isinstance(junit, TestSuite):
        junit = [junit]

    testsuites = []
    for testsuite in junit:
        if not hasattrs(testsuite, [
                'name',
                'tests',
                'failures',
                'errors'
        ]):
            print('\t\tCANNOT PARSE SUITES')
            if 'lintian' not in artifact_url:
                pdb.set_trace()
            return False
        try:
            suite = {
                'url': artifact_url,
                'name': testsuite.name,
                'timestamp': get_timestamp(testsuite),
                'time': get_time(testsuite),
                'tests': int(testsuite.tests),
                'failures': int(testsuite.failures),
                'errors': int(testsuite.errors),
                'skipped': get_skipped(testsuite),
                'testcases': []
            }
        except:
            pdb.set_trace()
            raise
        for testcase in testsuite:
            if not hasattrs(testcase, [
                    'name',
                    'result'
            ]):
                print('\t\tCANNOT PARSE CASES')
                pdb.set_trace()
                return False
            suite['testcases'].append({
                'classname': get_classname(testcase),
                'name': testcase.name,
                'time': get_time(testcase),
                'failure': len([x for x in testcase.result if isinstance(x, Failure)]) > 0,
                'skipped': len([x for x in testcase.result if isinstance(x, Skipped)]) > 0,
            })
        testsuites.append(suite)

    return testsuites


def parse_artifacts(artifacts):
    parsed_artifacts = []
    for artifact in artifacts:
        print(f'\tPARSING: "{artifact}"')
        r = requests.get(artifact)
        if r.status_code != 200:
            # Dunno. fuck it.
            continue
        try:
            junit = JUnitXml.fromstring(r.text.encode('utf8'))
            junit_output = parse_junit(artifact, junit)
            if junit_output:
                parsed_artifacts.append(junit_output)
        except (junitparser.junitparser.JUnitXmlError, lxml.etree.XMLSyntaxError) as e:
            print(f'\tNon-junit xml: "{artifact}"')
            continue

    return parsed_artifacts


def get_git_commit(actions):
    for action in actions:
        revs = action.get('buildsByBranchName')
        if not revs:
            continue

        heads = [*revs.keys()]
        if 'detatched' in heads and revs.get('detatched') is not None:
            return revs['detatched']['revision']['SHA1']
        for rev in heads:
            if rev == '_class':
                continue
            return revs[rev]['revision']['SHA1']
    return None


def get_zuul_param(actions, val):
    for action in actions:
        params = action.get('parameters')
        if not params:
            continue
        for param in params:
            if param.get('name', '') == val:
                return param['value']
    return None


def get_repo(build):
    return get_zuul_param(build['actions'], 'ZUUL_PROJECT')


def get_branch(build):
    branch = get_zuul_param(build['actions'], 'ZUUL_BRANCH')
    if not branch:
        # Why are we so fucking terrible at everything
        # <https://integration.wikimedia.org/ci/job/integration-config-qa>
        branch = get_zuul_param(build['actions'], 'ZUUL_REF')
    return branch


def get_commit(build):
    sha = get_git_commit(build['actions'])
    if not sha:
        # Why are we so fucking terrible at everything
        # <https://integration.wikimedia.org/ci/job/integration-config-qa>
        sha = get_zuul_param(build['actions'], 'ZUUL_COMMIT')
    if not sha:
        # WHY ARE WE SO FUCKING TERRIBLE AT FUCKING EVERYTHING
        # <https://integration.wikimedia.org/ci/job/quibble-daily-Cognate-vendor-mysql-php72-docker/>
        sha = get_zuul_param(build['actions'], 'ZUUL_REF')
    return sha


def parse_build(build):
    artifacts = [x['relativePath'] for x in build['artifacts'] if x['relativePath'].endswith('.xml')]
    if not artifacts:
        return False
    junit_results = parse_artifacts(
        [os.path.join(build['url'], 'artifact', xml) for xml in artifacts]
    )
    if not junit_results:
        print('\tNO JUNIT FILES IN BUILD')
        return False  # If here were no junit files in one build, probably none in another either
    return {
        'id': build['id'],
        'timestamp': build['timestamp'],
        'time': build['duration'],
        'url': build['url'],
        'host': build['builtOn'],
        'repo': get_repo(build),
        'branch': get_branch(build),
        'commit': get_commit(build),
        'testsuites': junit_results,
    }


def setup_db():
    """
    setup the database file if it doesn't exist
    """
    conn = sqlite3.connect(DB_PATH)
    # If the db didn't exist at the start of the function, do the setup
    crs = conn.cursor()

    crs.execute('''
        CREATE TABLE IF NOT EXISTS jobs (
            id INTEGER PRIMARY KEY,
            name TEXT UNIQUE NOT NULL,
            url TEXT UNIQUE NOT NULL
        );
    ''')
    crs.execute('''
        CREATE TABLE IF NOT EXISTS builds (
            id INTEGER PRIMARY KEY,
            timestamp INTEGER NOT NULL,
            time REAL NOT NULL,
            build_id INTEGER NOT NULL,
            url TEXT UNIQUE NOT NULL,
            repo TEXT NOT NULL,
            branch TEXT NOT NULL,
            sha TEXT NOT NULL,
            host TEXT NOT NULL,
            job_id INTEGER NOT NULL,
            UNIQUE(build_id,job_id),
            FOREIGN KEY(job_id) REFERENCES jobs(id)
        );
    ''')
    # <testsuite name="Smoke_test_for_search" timestamp="2021-09-23T02:17:41" time="5.678" tests="3" failures="0" errors="0" skipped="0">
    #     <properties>
    #         <property name="specId" value="0"/>
    #         <property name="suiteName" value="Smoke test for search"/>
    #         <property name="capabilities" value="chrome.90_0_4430_212.linux"/>
    #         <property name="file" value="./tests/selenium/specs/smoke_test.js"/>
    # </properties>
    crs.execute('''
        CREATE TABLE IF NOT EXISTS testsuites (
            id INTEGER PRIMARY KEY,
            build_id INTEGER NOT NULL,
            url TEXT NOT NULL,
            name TEXT,
            timestamp INTEGER,
            time REAL,
            tests INTEGER,
            failures INTEGER,
            errors INTEGER,
            skipped INTEGER,
            UNIQUE(url,name,timestamp),
            FOREIGN KEY(build_id) REFERENCES builds(id)
        );
    ''')

    #  <testcase classname="chrome.90_0_4430_212.linux.Smoke_test_for_search" name="Search_suggestions" time="2.092"><failure/></testcase>
    #  <testcase classname="chrome.90_0_4430_212.linux.Smoke_test_for_search" name="Fill_in_search_term_and_click_search" time="2.051"/>
    #  <testcase classname="chrome.90_0_4430_212.linux.Smoke_test_for_search" name="Search_with_accent_yields_result_page_with_accent" time="1.531"/>
    crs.execute('''
        CREATE TABLE IF NOT EXISTS testcases (
            id INTEGER PRIMARY KEY,
            testsuite_id INTEGER NOT NULL,
            classname TEXT,
            name TEXT,
            time REAL,
            failure INTEGER NOT NULL,
            skipped INTEGER NOT NULL,
            CHECK (failure IN (0, 1)),
            CHECK (skipped IN (0, 1)),
            FOREIGN KEY(testsuite_id) REFERENCES testsuites(id)
        );
    ''')
    conn.commit()
    return conn


def parse_args():
    ap = argparse.ArgumentParser()
    ap.add_argument('-r', '--refresh', action='store_true', help='Refresh json')
    return ap.parse_args()


if __name__ == '__main__':
    args = parse_args()
    conn = setup_db()
    crs = conn.cursor()
    out = []
    for job in fetch_jobs(refresh=args.refresh):
        name = job['name']
        print(f'CHECKING: "{name}"')
        builds = job.get('builds', [])
        for build in builds:
            if build_seen(crs, build['url']):
                continue
            parsed_build = parse_build(build)
            if not parsed_build:
                continue

            try:
                jn = job['name']
                print(f'\tINSERTING job: "{jn}"')
                job_id = insert_job(crs, job['name'], job['url'])
            except:
                pdb.set_trace()
                raise

            try:
                bi = parsed_build['id']
                print(f'\tINSERTING build: "{bi}"')
                crs.execute('''
                    INSERT INTO builds(
                        timestamp,
                        time,
                        build_id,
                        url,
                        repo,
                        branch,
                        sha,
                        host,
                        job_id
                    ) VALUES(?,?,?,?,?,?,?,?,?)''', (
                        parsed_build['timestamp'],
                        parsed_build['time'],
                        parsed_build['id'],
                        parsed_build['url'],
                        parsed_build['repo'],
                        parsed_build['branch'],
                        parsed_build['commit'],
                        parsed_build['host'],
                        job_id
                    )
                )
            except:
                pdb.set_trace()
                raise

            build_id = crs.lastrowid

            for testsuites in parsed_build['testsuites']:
                for testsuite in testsuites:
                    try:
                        tsn = testsuite['name']
                        print(f'\tINSERTING testsuite: "{tsn}"')
                        crs.execute('''
                            INSERT INTO testsuites(
                                build_id,
                                url,
                                name,
                                timestamp,
                                time,
                                tests,
                                failures,
                                errors,
                                skipped
                            ) VALUES(?,?,?,?,?,?,?,?,?)''', (
                                build_id,
                                testsuite['url'],
                                testsuite['name'],
                                testsuite['timestamp'],
                                testsuite['time'],
                                testsuite['tests'],
                                testsuite['failures'],
                                testsuite['errors'],
                                testsuite['skipped']
                            )
                        )
                    except:
                        pdb.set_trace()
                        raise


                    testsuite_id = crs.lastrowid

                    for testcase in testsuite['testcases']:
                        try:
                            tcn = testcase['name']
                            print(f'\tINSERTING testcase: "{tcn}"')
                            crs.execute('''
                                INSERT INTO testcases(
                                    testsuite_id,
                                    classname,
                                    name,
                                    time,
                                    failure,
                                    skipped
                                ) VALUES(?,?,?,?,?,?)''', (
                                    testsuite_id,
                                    testcase['classname'],
                                    testcase['name'],
                                    testcase['time'],
                                    testcase['failure'],
                                    testcase['skipped']
                                )
                            )
                        except:
                            pdb.set_trace()
                            raise

            print(f'\tCOMMITTING "{jn}:{bi}"')
            conn.commit()
